<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

use App\Application\Settings;
use App\Application\Service\Auth\AuthService;
use \League\Plates\Engine;

use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository\DefaultRepositoryFactory;

use \App\Application\Repository\UserRepository;
use Doctrine\ORM\ORMSetup;
use \Predis\Client as PredisClient;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([

        LoggerInterface::class => function(ContainerInterface $c) {
            $logger = new Logger('app');
            return $logger;
        },

        Settings::class => function(ContainerInterface $c) {
            return new Settings(
                [
                    'template' => [
                        'path' => $_ENV['APP_TEMPLATE_PATH'] ?? '../templates/default'
                    ],
                    'redis' => [
                        'scheme' => $_ENV['REDIS_SCHEME'] ?? 'tcp',
                        'host' => $_ENV['REDIS_HOST'] ?? 'redis',
                        'port' => $_ENV['REDIS_PORT'] ?? 6379,
                    ],
                    'db' => [
                        'name' => $_ENV['DB_NAME'] ?? 'auth_service',
                        'user' => $_ENV['DB_USER'] ?? 'postgres',
                        'pass' => $_ENV['DB_PASS'] ?? 'postgres',
                        'host' => $_ENV['DB_HOST'] ?? 'db',
                        'driver' => 'pdo_pgsql',
                    ]
                ],
            );
        },

        Engine::class => function(ContainerInterface $c) {
            $settings = $c->get(Settings::class);
            return new Engine($settings->get('template.path'));
        },

        UserRepository::class => function(ContainerInterface $c) {
            return (new DefaultRepositoryFactory)->getRepository($c->get(EntityManager::class), '\App\Application\Entity\User');
        },

        AuthService::class => function(ContainerInterface $c) {
            return new AuthService(
                $c->get(EntityManager::class),
                $c->get(PredisClient::class),
                $c->get(UserRepository::class),
            );
        },

        PredisClient::class => function(ContainerInterface $c) {
            $settings = $c->get(Settings::class);
            return new PredisClient([
                'scheme' => $settings->get('redis.scheme'),
                'host'   => $settings->get('redis.host'),
                'port'   => $settings->get('redis.port'),
            ]);
        },

        EntityManager::class => function(ContainerInterface $c) {
            $settings = $c->get(Settings::class);
            $connection = DriverManager::getConnection([
                'dbname' => $settings->get('db.name'),
                'user' => $settings->get('db.user'),
                'password' => $settings->get('db.pass'),
                'host' => $settings->get('db.host'),
                'driver' => 'pdo_pgsql',
                
            ]);

            $config = ORMSetup::createAttributeMetadataConfiguration(paths: [__DIR__.'/../src'], isDevMode: true);

            \Doctrine\DBAL\Types\Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');
            
            $em = new EntityManager($connection, $config);

            return $em;
        },

    ]);
};
