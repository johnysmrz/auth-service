<?php

declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;

use App\Application\Controller\LoginController;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    // $app->options('/{routes:.*}', function (Request $request, Response $response) {
    //     // CORS Pre-Flight OPTIONS Request Handler
    //     return $response;
    // });

    $app->group('/login', function (Group $group) {
        $group->get('', 'App\Application\Controller\LoginController:loginView');
        $group->post('', 'App\Application\Controller\LoginController:processLoginRequest');
    });

    $app->group('/api', function(Group $group) {
        $group->group('/user', function(Group $group) {
            // $group->get('/session/{session_id}', function (Request $request, Response $response) {
            //     $response->getBody()->write('{}');
            //     return $response->withHeader("Content-type", 'application/json');
            // });
            $group->get('/session/{sessionId}', 'App\Application\Controller\Api\ApiController:sessionId');
        });
    });
};
