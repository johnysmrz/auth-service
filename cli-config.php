<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\EntityManager;
use DI\ContainerBuilder;
use Doctrine\ORM\Tools\Console\EntityManagerProvider\SingleManagerProvider;

use Doctrine\Migrations\Configuration\Migration\ConfigurationArray;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\DependencyFactory;

require 'vendor/autoload.php';

// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();
$dependencies = require __DIR__ . '/app/dependencies.php';
$dependencies($containerBuilder);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// @TODO move to some reasonable config
$mc = new ConfigurationArray([
    'table_storage' => [
        'table_name' => 'doctrine_migration_versions',
        'version_column_name' => 'version',
        'version_column_length' => 191,
        'executed_at_column_name' => 'executed_at',
        'execution_time_column_name' => 'execution_time',
    ],

    'migrations_paths' => [
        'Migration' => __DIR__ . '/migrations',
    ],

    'all_or_nothing' => true,
    'transactional' => true,
    'check_database_platform' => true,
    'organize_migrations' => 'year_and_month',
    'connection' => null,
    'em' => null,
]);

$df = DependencyFactory::fromEntityManager($mc, new ExistingEntityManager($container->get(EntityManager::class)));

$commands = [
    new \Doctrine\Migrations\Tools\Console\Command\ExecuteCommand($df),
    new \Doctrine\Migrations\Tools\Console\Command\GenerateCommand($df),
    new \Doctrine\Migrations\Tools\Console\Command\LatestCommand($df),
    new \Doctrine\Migrations\Tools\Console\Command\MigrateCommand($df),
    new \Doctrine\Migrations\Tools\Console\Command\DiffCommand($df),
    new \Doctrine\Migrations\Tools\Console\Command\UpToDateCommand($df),
    new \Doctrine\Migrations\Tools\Console\Command\StatusCommand($df),
    new \Doctrine\Migrations\Tools\Console\Command\VersionCommand($df),
];

ConsoleRunner::run(
    new SingleManagerProvider($container->get(EntityManager::class)),
    $commands
);