<html>
<head>
    <title>
        <?= $this->escape($title) ?>
    </title>
</head>
<body>

<div id="page">
    <?= $this->section('page') ?>
</div>

</body>
</html>