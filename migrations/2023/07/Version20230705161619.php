<?php

declare(strict_types=1);

namespace Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230705161619 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA auth_service');
        $this->addSql('CREATE TABLE auth_service.t_group (id_group UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id_group))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B9A86A135E237E06 ON auth_service.t_group (name)');
        $this->addSql('COMMENT ON COLUMN auth_service.t_group.id_group IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE auth_service.j_group_permission (id_group UUID NOT NULL, id_permission UUID NOT NULL, PRIMARY KEY(id_group, id_permission))');
        $this->addSql('CREATE INDEX IDX_D398F748834505F5 ON auth_service.j_group_permission (id_group)');
        $this->addSql('CREATE INDEX IDX_D398F74845C3127D ON auth_service.j_group_permission (id_permission)');
        $this->addSql('COMMENT ON COLUMN auth_service.j_group_permission.id_group IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN auth_service.j_group_permission.id_permission IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE auth_service.t_permission (id_permission UUID NOT NULL, p_code VARCHAR(255) NOT NULL, p_description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id_permission))');
        $this->addSql('COMMENT ON COLUMN auth_service.t_permission.id_permission IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE auth_service.t_user (id_user UUID NOT NULL, u_username VARCHAR(255) NOT NULL, u_password VARCHAR(255) DEFAULT NULL, u_enabled BOOLEAN DEFAULT true NOT NULL, PRIMARY KEY(id_user))');
        $this->addSql('COMMENT ON COLUMN auth_service.t_user.id_user IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE auth_service.j_user_group (id_user UUID NOT NULL, id_group UUID NOT NULL, PRIMARY KEY(id_user, id_group))');
        $this->addSql('CREATE INDEX IDX_8FE349086B3CA4B ON auth_service.j_user_group (id_user)');
        $this->addSql('CREATE INDEX IDX_8FE34908834505F5 ON auth_service.j_user_group (id_group)');
        $this->addSql('COMMENT ON COLUMN auth_service.j_user_group.id_user IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN auth_service.j_user_group.id_group IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE auth_service.j_group_permission ADD CONSTRAINT FK_D398F748834505F5 FOREIGN KEY (id_group) REFERENCES auth_service.t_group (id_group) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE auth_service.j_group_permission ADD CONSTRAINT FK_D398F74845C3127D FOREIGN KEY (id_permission) REFERENCES auth_service.t_permission (id_permission) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE auth_service.j_user_group ADD CONSTRAINT FK_8FE349086B3CA4B FOREIGN KEY (id_user) REFERENCES auth_service.t_user (id_user) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE auth_service.j_user_group ADD CONSTRAINT FK_8FE34908834505F5 FOREIGN KEY (id_group) REFERENCES auth_service.t_group (id_group) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE auth_service.j_group_permission DROP CONSTRAINT FK_D398F748834505F5');
        $this->addSql('ALTER TABLE auth_service.j_group_permission DROP CONSTRAINT FK_D398F74845C3127D');
        $this->addSql('ALTER TABLE auth_service.j_user_group DROP CONSTRAINT FK_8FE349086B3CA4B');
        $this->addSql('ALTER TABLE auth_service.j_user_group DROP CONSTRAINT FK_8FE34908834505F5');
        $this->addSql('DROP TABLE auth_service.t_group');
        $this->addSql('DROP TABLE auth_service.j_group_permission');
        $this->addSql('DROP TABLE auth_service.t_permission');
        $this->addSql('DROP TABLE auth_service.t_user');
        $this->addSql('DROP TABLE auth_service.j_user_group');
    }
}
