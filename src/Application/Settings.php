<?php

declare(strict_types=1);

namespace App\Application;

use Psr\Log\LoggerInterface;

class Settings {
    public function __construct(protected array $settings) {

    }

    public function get(string $key, mixed $default = Null): mixed {
        $keyChunks = \explode('.', $key);
        $settingsBranch = $this->settings;

        for($i = 0; $i < \sizeof($keyChunks); $i++) {
            $currentChunk = $keyChunks[$i];
            if (isset($settingsBranch[$currentChunk])) {
                $settingsBranch = $settingsBranch[$currentChunk];
                if ($i == \sizeof($keyChunks) - 1) {
                    return $settingsBranch;
                }
            }
        }
        return $default;
    }

}