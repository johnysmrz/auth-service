<?php

declare(strict_types=1);

namespace App\Application\Repository;

use App\Application\Entity\User;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\UuidInterface;

class UserRepository extends EntityRepository {

    public function oneByUsername(string $username): ?User {
        return $this->findOneBy(['username' => $username]);
    }

    public function oneById(UuidInterface $uuid): ?User {
        return $this->findOneBy(['id' => $uuid]);
    }

}