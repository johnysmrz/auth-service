<?php

declare(strict_types=1);

namespace App\Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Ramsey\Uuid\Uuid;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;

use \App\Application\Repository\GroupRepository;

#[Table('t_group', schema: 'auth_service')]
#[Entity(repositoryClass: GroupRepository::class )]
class Group {

    #[Column(type: 'uuid', unique: true, nullable: false, name: 'id_group'), Id]
    protected \Ramsey\Uuid\UuidInterface $id;

    #[Column(type: 'string', unique: true, nullable: false)]
    protected string $name;

    #[ManyToMany(targetEntity: User::class, mappedBy: 'groups')]
    private Collection $users;

    #[JoinTable(name: 'auth_service.j_group_permission')]
    #[JoinColumn(name: 'id_group', referencedColumnName: 'id_group')]
    #[InverseJoinColumn(name: 'id_permission', referencedColumnName: 'id_permission')]
    #[ManyToMany(targetEntity: Permission::class, inversedBy: 'groups')]
    protected Collection $permissions;

    public function __construct(string $name) {
        $this->name = $name;
        $this->id = Uuid::uuid4();
        $this->users = new ArrayCollection();
        $this->permissions = new ArrayCollection();
    }

    public function getId(): \Ramsey\Uuid\UuidInterface {
        return $this->id;
    }

    public function setId(\Ramsey\Uuid\UuidInterface $id) {
        $this->id = $id;
        return $this;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    public function getUsers(): Collection {
        return $this->users;
    }

    public function getPermissions() {
        return $this->permissions;
    }

    public function addPermission(Permission $permission) {
        $this->permissions->add($permission);
    }

    public function removePermisssion(Permission $permission) {
        $this->permissions->removeElement($permission);
    }

}
