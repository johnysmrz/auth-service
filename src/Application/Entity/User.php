<?php

declare(strict_types=1);

namespace App\Application\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;

use Doctrine\ORM\Mapping\Id;

use \App\Application\Repository\UserRepository;

#[Table('t_user', schema: 'auth_service')]
#[Entity(repositoryClass: UserRepository::class )]
class User {

    #[Column(type: 'uuid', unique: true, nullable: false, name: 'id_user'), Id]
    protected \Ramsey\Uuid\UuidInterface $id;

    #[Column(type: 'string', nullable: false, name: 'u_username')]
    protected string $username;

    #[Column(type: 'string', nullable: true, name: 'u_password')]
    protected string $password;

    #[Column(type: 'boolean', nullable: false, name: 'u_enabled', options: ['default' => 't'])]
    protected bool $enabled = true;

    #[JoinTable(name: 'auth_service.j_user_group')]
    #[JoinColumn(name: 'id_user', referencedColumnName: 'id_user')]
    #[InverseJoinColumn(name: 'id_group', referencedColumnName: 'id_group')]
    #[ManyToMany(targetEntity: Group::class, inversedBy: 'users')]
    private Collection $groups;

    public function __construct(string $username) {
        $this->username = $username;
        $this->groups = new ArrayCollection();
    }

    public function getId(): \Ramsey\Uuid\UuidInterface {
        return $this->id;
    }

    public function setId(\Ramsey\Uuid\UuidInterface $id) {
        $this->id = $id;
        return $this;
    }

    public function getUsername(): string {
        return $this->username;
    }
    public function setUsername(string $username) {
        $this->username = $username;
        return $this;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function setPassword(string $password) {
        $this->password = $password;
        return $this;
    }

    public function isEnabled(): bool {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled) {
        $this->enabled = $enabled;
        return $this;
    }

    public function getGroups(): Collection {
        return $this->groups;
    }

    public function setGroups(Collection $groups) {
        $this->groups = $groups;
        return $this;
    }

    public function addGroup(Group $group) {
        $this->groups->add($group);
    }

    public function removeGroup(Group $group) {
        $this->groups->removeElement($group);
    }
}

