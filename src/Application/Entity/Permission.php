<?php

declare(strict_types=1);

namespace App\Application\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;

use Doctrine\ORM\Mapping\Id;

use \App\Application\Repository\PermissionRepository;

#[Table('t_permission', schema: 'auth_service')]
#[Entity(repositoryClass: PermissionRepository::class )]
class Permission {

    #[Column(type: 'uuid', unique: true, nullable: false, name: 'id_permission'), Id]
    protected \Ramsey\Uuid\UuidInterface $id;

    #[Column(type: 'string', nullable: false, name: 'p_code')]
    protected string $code;

    #[Column(type: 'string', nullable: true, name: 'p_description')]
    protected string $description;

    #[ManyToMany(targetEntity: Group::class, mappedBy: 'permissions')]
    private Collection $groups;

    public function __construct(string $code, ?string $description = null) {
        $this->groups = new ArrayCollection();
        $this->code = $code;
        $this->description = $description;
    }

    public function getId(): \Ramsey\Uuid\UuidInterface {
        return $this->id;
    }

    public function setId(\Ramsey\Uuid\UuidInterface $id) {
        $this->id = $id;
        return $this;
    }

    public function getGroups(): Collection {
        return $this->groups;
    }

    public function getCode(): string {
        return $this->code;
    }

    public function setCode(string $code) {
        $this->code = $code;
        return $this;
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setDescription(?string $description) {
        $this->description = $description;
        return $this;
    }
}

