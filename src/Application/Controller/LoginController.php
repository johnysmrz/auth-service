<?php

namespace App\Application\Controller;

use App\Application\Service\Auth\AuthService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Application\Service\Auth\DummyAuthenticator;

class LoginController {

    public function __construct(
        protected readonly \League\Plates\Engine $templates,
        protected readonly AuthService $authService) {

    }
    public function loginView(Request $request, Response $response) {
        $response->getBody()->write($this->templates->render('login/login', ['name' => 'Johny']));
        return $response;
    }

    public function processLoginRequest(Request $request, Response $response) {
        $requestArgs = $request->getParsedBody();

        if (isset($requestArgs['username']) && isset($requestArgs['password'])) {
            ['username' => $username, 'password' => $password] = $request->getParsedBody();
            $a = new DummyAuthenticator($username, $password);
            $this->authService->authenticate($a);
        }


        return $response->withStatus(302)->withHeader('Location', '/login');
    }

}
