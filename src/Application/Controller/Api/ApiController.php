<?php

namespace App\Application\Controller\Api;

use App\Application\Service\Auth\AuthService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Ramsey\Uuid\Uuid;

class ApiController {

    public function __construct(
        protected readonly \League\Plates\Engine $templates,
        protected readonly AuthService $authService) {

    }
    public function sessionId(Request $request, Response $response, array $args) {
        $sessionId = Uuid::fromString($args['sessionId']);
        $info = $this->authService->getSessionInfo($sessionId);

        $response->getBody()->write(\json_encode($info));
        return $response->withHeader('Content-type', 'application/json');
    }

}
