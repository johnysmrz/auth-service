<?php

declare(strict_types=1);

namespace App\Application\Service\Auth;

interface Authenticator {

    function authenticate(): bool;

    public function getUsername(): string;
}