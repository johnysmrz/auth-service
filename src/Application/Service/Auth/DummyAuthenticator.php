<?php

declare(strict_types=1);

namespace App\Application\Service\Auth;

class DummyAuthenticator implements Authenticator {

    protected array $authenticationErrors = [];

    public function __construct(
        protected readonly string $username,
        protected readonly string $password
    ) {
    }

    public function authenticate(): bool {
        if ($this->username == $this->password) {
            return true;
        }

        return false;
    }

    public function getAuthenticationErrors(): array {
        return $this->authenticationErrors;
    }

    public function getUsername(): string {
        return $this->username;
    }
}
