<?php

declare(strict_types=1);

namespace App\Application\Service\Auth;

class Result {

    public function __construct(
        public readonly bool $isValid,
        public readonly ?string $message = null,
    ) {}

}