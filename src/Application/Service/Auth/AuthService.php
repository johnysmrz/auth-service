<?php

declare(strict_types=1);

namespace App\Application\Service\Auth;

use App\Application\Entity\User;
use App\Application\Service\Auth\Authenticator;
use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Uuid;
use App\Application\Repository\UserRepository;
use Ramsey\Uuid\UuidInterface;

class AuthService {

    public function __construct(
        protected readonly EntityManager $em,
        protected readonly \Predis\Client $redis,
        protected readonly UserRepository $userRepository,
    ) {

    }

    public function authenticate(Authenticator $authenticator) {
        // basic time attack protection
        \usleep(\random_int(0, 300000));

        if ($authenticator->authenticate()) {
            $user = $this->userRepository->oneByUsername($authenticator->getUsername());
            if ($user === NULL) {
                throw new Security\AuthException(sprintf('User %s not found', $authenticator->getUsername()));
            }

            // get and update user permissions to redis
            $userPermissions = self::getUserPermissions($user);
            if (\sizeof($userPermissions)) {
                $this->redis->sadd(self::permissionKey($user), $userPermissions);
            }

            // is user enabled and allowed to login?
            if (!$user->isEnabled()) {
                throw new Security\AuthException(sprintf('User %s not enabled', $authenticator->getUsername()));
            }

            if ($this->redis->sismember(self::permissionKey($user), 'LOGIN') === 0) {
                throw new Security\AuthException(sprintf('User %s does not have permission LOGIN', $authenticator->getUsername()));
            }

            // everything seems to be ok, lets create session
            $newSessionId = Uuid::uuid4();

            $this->redis->set(self::sessionKey($newSessionId), $user->getId());
            $this->redis->expire(self::sessionKey($newSessionId), 48 * 3600);
        } else {
            
        }
    }

    public function getSessionInfo(UuidInterface $uuid): array {
        $userId = $this->redis->get(self::sessionKey($uuid));
        if ($userId == 0) {
            throw new Security\SessionException("Session $uuid not found");
        }

        $user = $this->userRepository->oneById(Uuid::fromString($userId));

        $info = [
            'sessionId' => $uuid,
            'userId' => $user->getId(),
            'permissions' => $this->redis->smembers(self::permissionKey($user)),
            'expireAt' => $this->redis->expiretime(self::sessionKey($uuid)),
        ];

        return $info;
    }

    public static function sessionKey(UuidInterface $uuid): string {
        return "session:$uuid";
    }

    public static function permissionKey(User $user): string {
        return \sprintf('permissions:%s', $user->getId());
    }

    public static function getUserPermissions(User $user): array {
        $userPermissions = [];
        foreach($user->getGroups() as $group) {
            foreach($group->getPermissions() as $permission) {
                \array_push($userPermissions, $permission->getCode());
            }
        }
        return $userPermissions;
    }


}