<?php

use PHPUnit\Framework\TestCase;

use App\Application\Settings;

class SettingsUnitTest extends TestCase {
    function testGet() {
        $settings = new Settings([
            'level1string' => 'ehlo', 
            'level1bool_true' => true,
            'level1bool_false' => false,
            'level1int' => 123,
            'level1float' => 3.141592,
            'level1' => [
                'level2string' => 'level2 ehlo',
                'level2' => [
                    'level3string' => 'level3 ehlo',
                ],
            ],
        ]);

        $this->assertEquals($settings->get('level1string'), 'ehlo');
        $this->assertEquals($settings->get('level1bool_true'), true);
        $this->assertEquals($settings->get('level1bool_false'), false);
        $this->assertEquals($settings->get('level1int'), 123);
        $this->assertEquals($settings->get('level1float'), 3.141592);
        $this->assertEquals($settings->get('level1.level2string'), 'level2 ehlo');
        $this->assertEquals($settings->get('level1.level2.level3string'), 'level3 ehlo');
    }

    function testGetDefaults() {
        $settings = new Settings([]);

        $this->assertEquals($settings->get('invalid', 'default'), 'default');
        $this->assertEquals($settings->get('invalid.level2', 'default'), 'default');
        $this->assertEquals($settings->get('invalid', 123), 123);
    }
}